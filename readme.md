# Powerball

A 3D FPS sports game currently in development.

https://www.youtube.com/watch?v=9ByDRAlQtZ4


## Compiling Requirements
* Java
* SteveTech1 (https://bitbucket.org/SteveSmith16384/stetech1)

If you are trying to compile this, you'll probably want to use the development branch of SteveTech1, since that's what I usually develop against.


## Credits
Written by Steve Smith ( http://twitter.com/stephencsmith/ )
Applause by https://opengameart.org/content/applause
Animated Human courtesy of Quaternius (http://quaternius.com/)
Minecraft-style human by Hanprogramer (https://www.blendswap.com/blends/view/88855)


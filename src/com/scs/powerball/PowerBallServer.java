package com.scs.powerball;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.jme3.math.Vector3f;
import com.jme3.system.JmeContext;
import com.scs.powerball.entities.Ball;
import com.scs.powerball.entities.BallLauncher;
import com.scs.powerball.entities.Floor;
import com.scs.powerball.entities.MapBorder;
import com.scs.powerball.entities.PlayerServerAvatar;
import com.scs.powerball.entities.Scorebox;
import com.scs.powerball.entities.StaticWall;
import com.scs.powerball.networking.BallStatusMessage;
import com.scs.powerball.networking.PlayerScoredMessage;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.data.SimplePlayerData;
import com.scs.stevetech1.entities.AbstractAvatar;
import com.scs.stevetech1.entities.AbstractServerAvatar;
import com.scs.stevetech1.server.AbstractSimpleGameServer;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.IAbility;

public class PowerBallServer extends AbstractSimpleGameServer {

	private static final int TENNIS_MAP_WIDTH = 5;
	private static final int TENNIS_MAP_DEPTH = 25;

	private static final int WARFARE_MAP_SIZE = 20;

	private static AtomicInteger nextSideNum;

	public static void main(String[] args) {
		try {
			new PowerBallServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private PowerBallServer() throws IOException {
		super(PowerBallStaticData.DEFAULT_PORT);

		collisionValidator = new PowerballCollisionValidator();

		start(JmeContext.Type.Headless);
	}


	@Override
	public void moveAvatarToStartPosition(AbstractAvatar avatar) {
		if (PowerBallStaticData.GAME_MODE == PowerBallStaticData.GM_TENNIS) {
			switch (avatar.side) {
			case 1:
				avatar.setWorldTranslation(3, 3f, 3);
				break;
			case 2:
				avatar.setWorldTranslation(3, 3f, TENNIS_MAP_DEPTH-3);
				break;
			}
		} else {
			avatar.setWorldTranslation(3, 3f, 3+(avatar.getSide()*3));
		}
	}


	@Override
	public void simpleUpdate(float tpfSecs) {
		super.simpleUpdate(tpfSecs);

		if (PowerBallStaticData.DEBUG_BALL_NOT_LAUNCHING) {
			for (int i=0 ; i<this.entitiesForProcessing.size() ; i++) {
				IEntity e = this.entitiesForProcessing.get(i);
				if (!e.isMarkedForRemoval()) {
					if (e instanceof Ball) {
						Ball ball = (Ball)e;
						if (ball.getHolder() != null) {
							PlayerServerAvatar a = (PlayerServerAvatar)ball.getHolder();
							if (a.getBall() == null) {
								Globals.pe("Inconsistent player and ball");
							}
						}
					}
				}
			}
		}
	}


	@Override
	protected void createGame() {
		nextSideNum = new AtomicInteger(1);

		if (PowerBallStaticData.GAME_MODE == PowerBallStaticData.GM_TENNIS) {

			Floor floor = new Floor(this, getNextEntityID(), 0, 0, 0, TENNIS_MAP_WIDTH, .5f, TENNIS_MAP_DEPTH, "Textures/neon1.jpg");
			this.actuallyAddEntity(floor);

			Floor ceiling = new Floor(this, getNextEntityID(), 0, 4, 0, TENNIS_MAP_WIDTH, .5f, TENNIS_MAP_DEPTH, "Textures/neon1.jpg");
			this.actuallyAddEntity(ceiling);

			Ball ball = new Ball(this, getNextEntityID(), TENNIS_MAP_WIDTH/2, 1, TENNIS_MAP_DEPTH/2);
			this.actuallyAddEntity(ball);
			ball.simpleRigidBody.setLinearVelocity(new Vector3f(.5f,  .5f,  5f));

			// Map border
			MapBorder borderL = new MapBorder(this, getNextEntityID(), 0, 0, 0, TENNIS_MAP_DEPTH, Vector3f.UNIT_Z);
			this.actuallyAddEntity(borderL);
			MapBorder borderR = new MapBorder(this, getNextEntityID(), TENNIS_MAP_WIDTH+MapBorder.BORDER_WIDTH, 0, 0, TENNIS_MAP_DEPTH, Vector3f.UNIT_Z);
			this.actuallyAddEntity(borderR);

			Scorebox borderBack = new Scorebox(this, getNextEntityID(), 0, 0, TENNIS_MAP_DEPTH, TENNIS_MAP_DEPTH, Vector3f.UNIT_X, (byte)1);
			this.actuallyAddEntity(borderBack);
			Scorebox borderFront = new Scorebox(this, getNextEntityID(), 0, 0, -MapBorder.BORDER_WIDTH, TENNIS_MAP_DEPTH, Vector3f.UNIT_X, (byte)2);
			this.actuallyAddEntity(borderFront);

		} else {

			int mapSize = 20;

			//StaticWall wall = new StaticWall(this, getNextEntityID(), 10, 0, 10, 1f, 10f, 1f, "Textures/neon1.jpg");
			//this.actuallyAddEntity(wall);

			Floor floor = new Floor(this, getNextEntityID(), 0, 0, 0, WARFARE_MAP_SIZE, .5f, WARFARE_MAP_SIZE, "Textures/neon1.jpg");
			this.actuallyAddEntity(floor);

			Ball ball = new Ball(this, getNextEntityID(), 3, 1, 3);
			this.actuallyAddEntity(ball);
			ball.simpleRigidBody.setLinearVelocity(new Vector3f(.5f,  .5f,  5f));

			// Map border
			MapBorder borderL = new MapBorder(this, getNextEntityID(), 0, 0, 0, WARFARE_MAP_SIZE, Vector3f.UNIT_Z);
			this.actuallyAddEntity(borderL);
			MapBorder borderR = new MapBorder(this, getNextEntityID(), WARFARE_MAP_SIZE+MapBorder.BORDER_WIDTH, 0, 0, WARFARE_MAP_SIZE, Vector3f.UNIT_Z);
			this.actuallyAddEntity(borderR);
			MapBorder borderBack = new MapBorder(this, getNextEntityID(), 0, 0, WARFARE_MAP_SIZE, WARFARE_MAP_SIZE, Vector3f.UNIT_X);
			this.actuallyAddEntity(borderBack);
			MapBorder borderFront = new MapBorder(this, getNextEntityID(), 0, 0, -MapBorder.BORDER_WIDTH, WARFARE_MAP_SIZE, Vector3f.UNIT_X);
			this.actuallyAddEntity(borderFront);

		}

	}


	@Override
	protected AbstractServerAvatar createPlayersAvatarEntity(ClientData client, int entityid) {
		PlayerServerAvatar avatar = new PlayerServerAvatar(this, client, client.remoteInput, entityid);

		IAbility abilityGun = new BallLauncher(this, getNextEntityID(), client.getPlayerID(), avatar, entityid, (byte)0);
		this.actuallyAddEntity(abilityGun);

		return avatar;
	}


	@Override
	protected byte getWinningSideAtEnd() {
		int highestScore = -1;
		byte winningSide = -1;
		boolean draw = false;
		for(ClientData c : this.clientList.getClients()) {
			PowerBallSimplePlayerData spd = (PowerBallSimplePlayerData)c.playerData;
			if (spd.score > highestScore) {
				winningSide = spd.side;
				highestScore = spd.score;
				draw = false;
			} else if (spd.score == highestScore) {
				draw = true;
			}
		}
		if (draw) {
			return -1;
		}
		return winningSide;
	}


	@Override
	protected Class<? extends Object>[] getListofMessageClasses() {
		return new Class[] {PowerBallSimplePlayerData.class, BallStatusMessage.class, PlayerScoredMessage.class};
	}


	@Override
	public byte getSideForPlayer(ClientData client) {
		return (byte)nextSideNum.getAndIncrement();
	}


	@Override
	public int getMinSidesRequiredForGame() {
		return PowerBallStaticData.GAME_MODE == PowerBallStaticData.GM_TENNIS ? 2 : 1;
	}


	@Override
	protected SimplePlayerData createSimplePlayerData() {
		return new PowerBallSimplePlayerData();
	}


	@Override
	public boolean doWeHaveSpaces() {
		if (PowerBallStaticData.GAME_MODE == PowerBallStaticData.GM_TENNIS) {
			return super.clientList.getNumClients() < 2;
		} else {
			return true;
		}
	}


}


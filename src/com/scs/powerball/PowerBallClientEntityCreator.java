package com.scs.powerball;

import com.jme3.math.Vector3f;
import com.scs.powerball.entities.Ball;
import com.scs.powerball.entities.BallLauncher;
import com.scs.powerball.entities.Floor;
import com.scs.powerball.entities.MapBorder;
import com.scs.powerball.entities.OtherPlayerAvatar;
import com.scs.powerball.entities.PlayerClientAvatar;
import com.scs.powerball.entities.Scorebox;
import com.scs.powerball.entities.StaticWall;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.entities.AbstractClientAvatar;
import com.scs.stevetech1.entities.AbstractOtherPlayersAvatar;
import com.scs.stevetech1.entities.DebuggingSphere;
import com.scs.stevetech1.entities.ExplosionShard;
import com.scs.stevetech1.netmessages.NewEntityData;
import com.scs.stevetech1.server.Globals;

public class PowerBallClientEntityCreator {

	public static final int AVATAR = 1;
	public static final int FLOOR = 2;
	public static final int BALL = 3;
	public static final int BALL_LAUNCHER = 4;
	public static final int SCOREBOX = 5;
	public static final int MAP_BORDER = 6;
	public static final int STATIC_WALL = 7;

	public PowerBallClientEntityCreator() {
		super();
	}


	public IEntity createEntity(PowerBallClient game, NewEntityData msg) {
		int id = msg.entityID;

		Vector3f pos = (Vector3f)msg.data.get("pos");

		switch (msg.type) {
		case AVATAR:
		{
			int playerID = (int)msg.data.get("playerID");
			byte side = (byte)msg.data.get("side");
			String playersName = (String)msg.data.get("playersName");

			if (playerID == game.playerID) {
				AbstractClientAvatar avatar = new PlayerClientAvatar(game, id, game.input, game.getCamera(), id, pos.x, pos.y, pos.z, side);
				return avatar;
			} else {
				// Create a simple avatar since we don't control these
				AbstractOtherPlayersAvatar avatar = new OtherPlayerAvatar(game, id, pos.x, pos.y, pos.z, side, playersName);
				return avatar;
			}
		}

		case BALL:
		{
			Ball ball = new Ball(game, id, pos.x, pos.y, pos.z);
			game.ball = ball;
			return ball;
		}

		case FLOOR:
		{
			Vector3f size = (Vector3f)msg.data.get("size");
			String tex = (String)msg.data.get("tex");
			Floor floor = new Floor(game, id, pos.x, pos.y, pos.z, size.x, size.y, size.z, tex);
			return floor;
		}

		case SCOREBOX:
		{
			float size = (float)msg.data.get("size");
			Vector3f dir = (Vector3f)msg.data.get("dir");
			byte side = (byte)msg.data.get("side");
			Scorebox scorebox = new Scorebox(game, id, pos.x, pos.y, pos.z, size, dir, side);
			return scorebox;
		}

		case MAP_BORDER:
		{
			Vector3f dir = (Vector3f)msg.data.get("dir");
			float size = (float)msg.data.get("size");
			MapBorder border = new MapBorder(game, id, pos.x, pos.y, pos.z, size, dir);
			return border;
		}

		case STATIC_WALL:
		{
			Vector3f size = (Vector3f)msg.data.get("size");
			String tex = (String)msg.data.get("tex");
			StaticWall igloo = new StaticWall(game, id, pos.x, pos.y, pos.z, size.x, size.y, size.z, tex);
			return igloo;
		}

		case BALL_LAUNCHER: 
		{
			int ownerid = (int)msg.data.get("ownerid");
			byte num = (byte)msg.data.get("num");
			int playerID = (int)msg.data.get("playerID");
			BallLauncher gl = new BallLauncher(game, id, playerID, null, ownerid, num);
			return gl;
		}

		case Globals.DEBUGGING_SPHERE:
		{
			DebuggingSphere hill = new DebuggingSphere(game, id, pos.x, pos.y, pos.z, true, false);
			return hill;
		}

		case Globals.EXPLOSION_SHARD:
		{
			Vector3f forceDirection = (Vector3f) msg.data.get("forceDirection");
			float size = (float) msg.data.get("size");
			String tex = (String) msg.data.get("tex");
			ExplosionShard expl = new ExplosionShard(game, pos.x, pos.y, pos.z, size, forceDirection, tex);
			return expl;
		}

		default:
			throw new RuntimeException("Unknown entity type: " + msg.type);
		}
	}

}


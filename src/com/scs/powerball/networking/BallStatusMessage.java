package com.scs.powerball.networking;

import com.jme3.network.serializing.Serializable;
import com.scs.powerball.entities.Ball;
import com.scs.stevetech1.netmessages.MyAbstractMessage;

@Serializable
public class BallStatusMessage extends MyAbstractMessage {

	public boolean harmful;
	public boolean heldByAvatar; // or released
	public int avatarID;
	
	public BallStatusMessage() {
		super(true, true);
	}
	
	public BallStatusMessage(Ball ball) {
		this();
		
		heldByAvatar = ball.getHolder() != null;;
		avatarID = ball.getHolder() != null ? ball.getHolder().getID() : -1;
		harmful = ball.harmful;
	}
	
}

package com.scs.powerball.networking;

import com.jme3.network.serializing.Serializable;
import com.scs.stevetech1.netmessages.MyAbstractMessage;

@Serializable
public class PlayerScoredMessage extends MyAbstractMessage {
	
	public int playerScorer, playerHit;
	
	public PlayerScoredMessage() {
		super(true, true);
	}


	public PlayerScoredMessage(int _playerScorer, int _playerHit) {
		this();
		
		playerScorer = _playerScorer;
		playerHit = _playerHit;
	}

}

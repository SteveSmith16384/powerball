package com.scs.powerball;

import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.scs.powerball.entities.Ball;
import com.scs.powerball.entities.PlayerClientAvatar;
import com.scs.powerball.networking.BallStatusMessage;
import com.scs.powerball.networking.PlayerScoredMessage;
import com.scs.stevetech1.client.AbstractGameClient;
import com.scs.stevetech1.client.AbstractSimpleGameClient;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.netmessages.MyAbstractMessage;
import com.scs.stevetech1.netmessages.NewEntityData;
import com.scs.stevetech1.server.Globals;

public class PowerBallClient extends AbstractSimpleGameClient {

	private PowerBallClientEntityCreator entityCreator;
	private PowerBallHUD hud;
	private DirectionalLight sun;
	public Ball ball;

	public static void main(String[] args) {
		try {
			new PowerBallClient("localhost", PowerBallStaticData.DEFAULT_PORT, "My Name");
		} catch (Exception e) {
			Globals.p("Error: " + e);
			e.printStackTrace();
		}
	}


	private PowerBallClient(String gameIpAddress, int gamePort, String _playerName) {
		super("PowerBall", gameIpAddress, gamePort, _playerName);

		collisionValidator = new PowerballCollisionValidator();

		start();		
	}


	@Override
	public void simpleInitApp() {
		super.simpleInitApp();

		entityCreator = new PowerBallClientEntityCreator();

		hud = new PowerBallHUD(this, this.getCamera());
		this.getGuiNode().attachChild(hud);

		// Add shadows
		final int SHADOWMAP_SIZE = 1024*2;
		DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(getAssetManager(), SHADOWMAP_SIZE, 2);
		dlsr.setLight(sun);
		this.viewPort.addProcessor(dlsr);

	}


	@Override
	protected void setUpLight() {
		AmbientLight al = new AmbientLight();
		al.setColor(ColorRGBA.White);
		getGameNode().addLight(al);

		sun = new DirectionalLight();
		sun.setColor(ColorRGBA.Yellow);
		sun.setDirection(new Vector3f(.5f, -1f, .5f).normalizeLocal());
		getGameNode().addLight(sun);
	}


	@Override
	public void simpleUpdate(float tpf_secs) {
		super.simpleUpdate(tpf_secs);

		hud.processByClient(this, tpf_secs);
	}


	@Override
	protected IEntity actuallyCreateEntity(AbstractGameClient client, NewEntityData msg) {
		return entityCreator.createEntity((PowerBallClient)client, msg);
	}


	@Override
	protected boolean handleMessage(MyAbstractMessage message) {
		if (message instanceof BallStatusMessage) {
			BallStatusMessage msg = (BallStatusMessage)message;
			if (msg.heldByAvatar && msg.avatarID == this.currentAvatarID) { // Only lock it to an avatar if it's our avatar
				ball.setHolder((PlayerClientAvatar)this.entities.get(msg.avatarID));
			} else {
				ball.setHolder(null);
			}
			ball.setHarmfulTex(msg.harmful);
			return true;
			
		} else if (message instanceof PlayerScoredMessage) {
			PlayerScoredMessage psm = (PlayerScoredMessage)message;
			if (psm.playerHit == this.currentAvatarID) {
				hud.showDamageBox();
			} else if (psm.playerScorer == this.currentAvatarID) {
				hud.showCollectBox();
			}
			return true;
			
		} else {
			return super.handleMessage(message);
		}
	}


	@Override
	protected Class<? extends Object>[] getListofMessageClasses() {
		return new Class[] {PowerBallSimplePlayerData.class, BallStatusMessage.class, PlayerScoredMessage.class};
	}


	@Override
	protected void showDamageBox() {
		this.hud.showDamageBox();
	}


	@Override
	protected void showMessage(String msg) {
		this.hud.appendToLog(msg);
	}


	@Override
	protected void appendToLog(String msg) {
		this.hud.appendToLog(msg);
	}


}

package com.scs.powerball.entities;

import java.util.HashMap;

import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.powerball.networking.BallStatusMessage;
import com.scs.powerball.networking.PlayerScoredMessage;
import com.scs.simplephysics.SimpleRigidBody;
import com.scs.stevetech1.client.IClientApp;
import com.scs.stevetech1.components.ICanShoot;
import com.scs.stevetech1.components.INotifiedOfCollision;
import com.scs.stevetech1.components.IProcessByClient;
import com.scs.stevetech1.components.IRewindable;
import com.scs.stevetech1.entities.AbstractAvatar;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.server.AbstractGameServer;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.IEntityController;

public class Ball extends PhysicalEntity implements INotifiedOfCollision, IProcessByClient, IRewindable {

	private static final float LAUNCH_FORCE = 15f;
	public static final int NUM_BOUNCES = 5;
	private static final float HOLD_DIST = 0.6f;

	public boolean harmful = false;
	private AbstractAvatar holder = null; // PlayerServerAvatar on server, PlayerClientAvatar on client
	public PlayerServerAvatar thrower = null;
	public int numBouncesLeft = 5;

	private Material material;
	private Texture tex_harmless, tex_harmful;

	public Ball(IEntityController _game, int id, float x, float y, float z) {
		super(_game, id, PowerBallClientEntityCreator.BALL, "Ball", true, false, true);

		if (_game.isServer()) {
			creationData = new HashMap<String, Object>();
		}

		Mesh sphere = new Sphere(8, 8, .2f, true, false);
		Geometry ball_geo = new Geometry("DebuggingSphere", sphere);
		ball_geo.setShadowMode(ShadowMode.CastAndReceive);

		TextureKey key3 = new TextureKey( "Textures/football.jpg");
		tex_harmless = game.getAssetManager().loadTexture(key3);

		TextureKey key4 = new TextureKey( "Textures/yellowsun.jpg");
		tex_harmful = game.getAssetManager().loadTexture(key4);

		material = new Material(game.getAssetManager(),"Common/MatDefs/Light/Lighting.j3md");
		material.setTexture("DiffuseMap", tex_harmless);
		ball_geo.setMaterial(material);

		this.mainNode.attachChild(ball_geo);
		this.mainNode.setLocalTranslation(x, y, z);

		this.simpleRigidBody = new SimpleRigidBody<PhysicalEntity>(this, game.getPhysicsController(), game.isServer(), this);
		simpleRigidBody.setBounciness(.9f);
		simpleRigidBody.setAerodynamicness(1f);

		ball_geo.setUserData(Globals.ENTITY, this);

	}


	public void launch(PlayerServerAvatar player) {
		thrower = player;
		ICanShoot ic = (ICanShoot)player;
		Vector3f dir = ic.getShootDir().clone();
		GetSimpleRigidBody().setLinearVelocity(dir.multLocal(LAUNCH_FORCE));
		setHolder(null);
		this.GetSimpleRigidBody().setSolid(true);
		player.setBall(null);
		this.numBouncesLeft = NUM_BOUNCES;

		AbstractGameServer server = (AbstractGameServer)game;
		server.sendMessageToInGameClients(new BallStatusMessage(this));

	}


	public void setHarmfulTex(boolean h) {
		if (h) {
			material.setTexture("DiffuseMap", tex_harmful);
		} else {
			material.setTexture("DiffuseMap", tex_harmless);
		}
	}


	public AbstractAvatar getHolder() {
		return this.holder;
	}


	public void setHolder(AbstractAvatar avatar) {
		if (game.isServer()) {
			if (this.holder != null && avatar == null) {
				this.harmful = true;
				AbstractGameServer server = (AbstractGameServer)game;
				server.sendMessageToInGameClients(new BallStatusMessage(this));
			}
		}
		this.holder = avatar;
		this.GetSimpleRigidBody().setSolid(false);
	}


	@Override
	public void notifiedOfCollision(PhysicalEntity collidedWith) {
		if (game.isServer()) {
			if (collidedWith instanceof Floor == false) {
				//Globals.p(this + " collided with " + collidedWith);
				if (collidedWith instanceof PlayerServerAvatar) {
					if (holder == null) {
						PlayerServerAvatar avatarHit = (PlayerServerAvatar) collidedWith;
						Globals.p(avatarHit + " has been hit!");
						if (avatarHit == thrower && this.numBouncesLeft == NUM_BOUNCES) {
							// Do nothing since we've just thrown it
							return;
						}
						AbstractGameServer server = (AbstractGameServer)game;
						if (this.harmful && thrower != null) {
							if (thrower != avatarHit) { // Only score if we didn't hit ourselves
								thrower.incScore(1);
							}
							server.sendMessageToInGameClients(new PlayerScoredMessage(thrower.getID(), avatarHit.getID()));
						} else {
							setHolder(avatarHit);
							avatarHit.setBall(this);
							server.sendMessageToInGameClients(new BallStatusMessage(this));
						}
					}
				}
			}
			if (this.harmful) {
				numBouncesLeft--;
				if (numBouncesLeft <= 0) {
					this.harmful = false;
					AbstractGameServer server = (AbstractGameServer)game;
					server.sendMessageToInGameClients(new BallStatusMessage(this));
				}
			}
		}
	}


	@Override
	public void processByServer(AbstractGameServer server, float tpfSecs) {
		if (this.holder != null) {
			this.GetSimpleRigidBody().resetForces();
			// Position us in front of the holder
			PlayerServerAvatar a = (PlayerServerAvatar)holder;
			Vector3f pos = a.getWorldTranslation().clone();
			Vector3f rot = a.getRotation();

			pos.x += rot.x * HOLD_DIST;
			pos.y += (rot.y * HOLD_DIST) + .3f;
			pos.z += rot.z * HOLD_DIST;
			this.setWorldTranslation(pos);
		} else {
			// Rewind and check for collisions
			this.rewindPositionTo(System.currentTimeMillis() - server.gameOptions.clientRenderDelayMillis);
			//EntityPositionData epd = this.historicalPositionData.calcPosition(System.currentTimeMillis() - server.gameOptions.clientRenderDelayMillis, false);
			//this.setWorldTranslation(epd.position);
			this.GetSimpleRigidBody().checkForCollisions(true);
			this.restorePosition();
		}

		super.processByServer(server, tpfSecs);
	}


	@Override
	public void fallenOffEdge() {
		this.setWorldTranslation(15f, 10f, 15f); // todo - check not something there
	}


	@Override
	public void processByClient(IClientApp client, float tpfSecs) {
		if (this.holder != null) {
			this.GetSimpleRigidBody().resetForces();
			// Position us in front of the holder
			PlayerClientAvatar a = (PlayerClientAvatar)holder;
			Vector3f pos = a.getWorldTranslation().clone();
			Vector3f rot = a.getRotation();

			pos.x += rot.x * HOLD_DIST;
			pos.y += (rot.y * HOLD_DIST) + .3f;
			pos.z += rot.z * HOLD_DIST;
			this.setWorldTranslation(pos);
		}		
	}

}


package com.scs.powerball.entities;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.powerball.PowerBallStaticData;
import com.scs.powerball.models.PlayerModel;
import com.scs.stevetech1.avatartypes.PersonAvatarControl;
import com.scs.stevetech1.client.AbstractGameClient;
import com.scs.stevetech1.components.IGetRotation;
import com.scs.stevetech1.entities.AbstractClientAvatar;
import com.scs.stevetech1.input.IInputDevice;

public class PlayerClientAvatar extends AbstractClientAvatar implements IGetRotation {

	public PlayerClientAvatar(AbstractGameClient _module, int _playerID, IInputDevice _input, Camera _cam, int eid, float x, float y, float z, byte side) {
		super(_module, PowerBallClientEntityCreator.AVATAR, _playerID, _input, _cam, eid, x, y, z, side, new PlayerModel(_module.getAssetManager(), side), new PersonAvatarControl(_module, _input, PowerBallStaticData.MOVE_SPEED, PowerBallStaticData.JUMP_FORCE));

		//simpleRigidBody.setGravity(0); // So they move exactly where we want, even when client jumps
	}

	@Override
	public Vector3f getRotation() {
		AbstractGameClient client = (AbstractGameClient)game;
		return client.getCamera().getDirection();
	}


}

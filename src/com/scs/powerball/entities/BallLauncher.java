package com.scs.powerball.entities;

import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.stevetech1.entities.AbstractAvatar;
import com.scs.stevetech1.netmessages.AbilityUpdateMessage;
import com.scs.stevetech1.shared.AbstractAbility;
import com.scs.stevetech1.shared.IAbility;
import com.scs.stevetech1.shared.IEntityController;

public class BallLauncher extends AbstractAbility implements IAbility {

	public BallLauncher(IEntityController _game, int _id, int _playerID, AbstractAvatar _owner, int _avatarID, byte num) {
		super(_game, _id, PowerBallClientEntityCreator.BALL_LAUNCHER, _playerID, _owner, _avatarID, num, "Ball Launcher", 1f);
	}


	@Override
	public boolean activate() {
		if (game.isServer()) {
			PlayerServerAvatar player = (PlayerServerAvatar)this.owner;
			Ball ball = player.getBall();
			if (ball != null) {
				ball.launch(player);
				return true;
			}
			return false;
		} else {
			return true;
		}
	}


	@Override
	public String getHudText() {
		return null;
	}


	@Override
	public void encode(AbilityUpdateMessage aum) {

	}


	@Override
	public void decode(AbilityUpdateMessage aum) {

	}

}

package com.scs.powerball.entities;

import java.util.HashMap;

import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.simplephysics.SimpleRigidBody;
import com.scs.stevetech1.components.INotifiedOfCollision;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.jme.JMEAngleFunctions;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.IEntityController;

public class Scorebox extends PhysicalEntity implements INotifiedOfCollision {

	public static final float BORDER_WIDTH = 2f;
	public static final float BORDER_HEIGHT = 5f;
	
	private byte side;

	public Scorebox(IEntityController _game, int id, float x, float y, float z, float size, Vector3f dir, byte _side) {
		super(_game, id, PowerBallClientEntityCreator.SCOREBOX, "Scorebox", false, true, false);

		side = _side;
		
		if (_game.isServer()) {
			creationData = new HashMap<String, Object>();
			creationData.put("size", size);
			creationData.put("dir", dir);
			creationData.put("side", side);
		}

		Box box1 = new Box(BORDER_WIDTH/2, BORDER_HEIGHT/2, size/2);
		Geometry geometry = new Geometry("MapBorderBox", box1);
		if (!_game.isServer()) { // Not running in server
			TextureKey key3 = null;
			if (side == 1) {
				key3 = new TextureKey("Textures/fence.png");
			} else if (side == 2) {
				key3 = new TextureKey("Textures/fence.png");
			} else {
				throw new RuntimeException("Invalid side: " + side);
			}
			key3.setGenerateMips(true);
			Texture tex3 = game.getAssetManager().loadTexture(key3);
			tex3.setWrap(WrapMode.Repeat);

			Material floor_mat = new Material(game.getAssetManager(),"Common/MatDefs/Light/Lighting.j3md");  // create a simple material
			floor_mat.setTexture("DiffuseMap", tex3);

			geometry.setMaterial(floor_mat);
		}

		geometry.setLocalTranslation(-BORDER_WIDTH/2, BORDER_HEIGHT/2, size/2);
		mainNode.attachChild(geometry);
		JMEAngleFunctions.rotateToWorldDirection(mainNode, dir);
		mainNode.setLocalTranslation(x, y, z);

		this.simpleRigidBody = new SimpleRigidBody<PhysicalEntity>(this, game.getPhysicsController(), false, this);
		simpleRigidBody.setModelComplexity(0);
		simpleRigidBody.setNeverMoves(true);

		geometry.setUserData(Globals.ENTITY, this);
	}

	@Override
	public void notifiedOfCollision(PhysicalEntity collidedWith) {
		// TODO Auto-generated method stub
		
	}

}

package com.scs.powerball.entities;

import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.powerball.models.PlayerModel;
import com.scs.stevetech1.entities.AbstractOtherPlayersAvatar;
import com.scs.stevetech1.shared.IEntityController;

public class OtherPlayerAvatar extends AbstractOtherPlayersAvatar {
	
	public OtherPlayerAvatar(IEntityController game, int eid, float x, float y, float z, byte side, String playerName) {
		super(game, PowerBallClientEntityCreator.AVATAR, eid, x, y, z, new PlayerModel(game.getAssetManager(), side), side, playerName);
	}


	@Override
	public void setAnimCode_ClientSide(int animCode) {
		model.setAnim(animCode);
	}


}

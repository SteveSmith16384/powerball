package com.scs.powerball.entities;

import com.scs.powerball.PowerBallClientEntityCreator;
import com.scs.powerball.PowerBallSimplePlayerData;
import com.scs.powerball.PowerBallStaticData;
import com.scs.powerball.models.PlayerModel;
import com.scs.stevetech1.avatartypes.PersonAvatarControl;
import com.scs.stevetech1.entities.AbstractServerAvatar;
import com.scs.stevetech1.input.IInputDevice;
import com.scs.stevetech1.server.AbstractGameServer;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.IEntityController;

public class PlayerServerAvatar extends AbstractServerAvatar {

	private static final float AUTOSHOOT_BALL_SECS = 3f;

	private Ball ball;
	private float autoShootBallTime = 0;

	public PlayerServerAvatar(IEntityController _module, ClientData client, IInputDevice _input, int eid) {
		super(_module, PowerBallClientEntityCreator.AVATAR, client, _input, eid, new PlayerModel(_module.getAssetManager(), client.getSide()), PowerBallStaticData.START_HEALTH, 0, new PersonAvatarControl(_module, _input, PowerBallStaticData.MOVE_SPEED, PowerBallStaticData.JUMP_FORCE));
	}


	@Override
	public void getReadyForGame() {
		super.getReadyForGame();

		PowerBallSimplePlayerData data = (PowerBallSimplePlayerData)this.client.playerData;
		data.score = 0;
	}


	@Override
	public void processByServer(AbstractGameServer server, float tpf) {
		super.processByServer(server, tpf);

		if (ball != null) {
			if (ball.isMarkedForRemoval()) {
				ball = null;
			} else {
				autoShootBallTime -= tpf;
				if (autoShootBallTime <= 0) {
					if (!PowerBallStaticData.DEBUG_BALL_COLLIDING) {
						this.ability[0].activate();
					}
				}
			}
		}
	}


	public void setBall(Ball b) {
		ball = b;
		if (b != null) {
			autoShootBallTime = AUTOSHOOT_BALL_SECS;
		} else {
			//Globals.p("Setting ball to null");
		}
	}


	public Ball getBall() {
		return ball;
	}

	/*
	@Override
	public void damaged(float amt, IEntity collider, String reason) {
		super.damaged(amt, collider, reason);
		/*
		if (collider instanceof FireballBullet) {
			FireballBullet sb = (FireballBullet)collider;
			AbstractGameServer server = (AbstractGameServer)game;
			server.sendExplosionShards(sb.getWorldTranslation(), 12, .8f, 1.2f, .005f, .02f, "Textures/yellowsun.jpg");
			this.avatarControl.jump(); // Also make them jump
		}
	}

/*
	@Override
	protected void setDied(IEntity killer, String reason) {
		super.setDied(killer, reason);

		if (killer != null && killer instanceof PlayerServerAvatar) {
			PlayerServerAvatar csp = (PlayerServerAvatar)killer;
			csp.incScore(1);
		}
	}
	 */

	public void incScore(int i) {
		PowerBallSimplePlayerData data = (PowerBallSimplePlayerData)this.client.playerData;
		data.score += i;
		AbstractGameServer server = (AbstractGameServer)game;
		server.sendSimpleGameDataToClients();
	}


}

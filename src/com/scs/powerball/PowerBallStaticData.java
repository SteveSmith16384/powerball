package com.scs.powerball;

public class PowerBallStaticData {
	
	public static final int GM_WARFARE = 0;
	public static final int GM_TENNIS = 1;
	
	public static final int GAME_MODE = GM_WARFARE;
	

	// Debugging
	public static final boolean DEBUG_BALL_COLLIDING = false;
	public static final boolean DEBUG_BALL_NOT_LAUNCHING = true;

	// Other
	public static final int DEFAULT_PORT = 6144;
	public static final boolean USE_MODELS_FOR_COLLISION = true;
	public static final float START_HEALTH = 100f;
	public static final float MOVE_SPEED = 3f;
	public static final float JUMP_FORCE = 8f;

}

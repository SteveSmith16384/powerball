package com.scs.powerball;

import com.scs.powerball.entities.Ball;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.AbstractCollisionValidator;

public class PowerballCollisionValidator extends AbstractCollisionValidator {


	@Override
	public boolean canCollide(PhysicalEntity pa, PhysicalEntity pb) {
		// Ball won't collide if someone holding it
		if (pa instanceof Ball) {
			Ball ball = (Ball)pa;
			if (ball.getHolder() != null) {
				return false;
			}
		} else if (pb instanceof Ball) {
			Ball ball = (Ball)pb;
			if (ball.getHolder() != null) {
				return false;
			}
		}

		if (PowerBallStaticData.DEBUG_BALL_COLLIDING) {
			if (pa instanceof Ball || pb instanceof Ball) {
				Globals.p("Ball colliding");
			}
		}
		return super.canCollide(pa, pb);
	}

}

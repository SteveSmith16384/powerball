package com.scs.powerball;

import com.jme3.network.serializing.Serializable;
import com.scs.stevetech1.data.SimplePlayerData;

@Serializable
public class PowerBallSimplePlayerData extends SimplePlayerData {

	public int score;
	public int resources;
	
	public PowerBallSimplePlayerData() {
		super();
	}
}
